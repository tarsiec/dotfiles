# My personal configuration files

![](img.png?raw=true)

These are the dotfiles for the programs I use in my Arch Linux system. The
amount of modification varies between the programs. And they are gathered here
mainly to be easier fot me to access them any time I want, without having to
keep them in my system. That being said, I'm quite proud of some configs
(mostly `emacs`,`nvim`, and `qtile`, although I'm improving
all of them) and hope you, stranger reading this, like them as well.

Note that right now (as my .xinitrc would suggest) I'm using
[dwm](https://github.com/t0maslb/dwm) as my wm and am using a solarized dark color scheme in all of
them (plus st and vim), while with qtile I use kitty and in both one
colorscheme called snazzy, which I also quite like.
